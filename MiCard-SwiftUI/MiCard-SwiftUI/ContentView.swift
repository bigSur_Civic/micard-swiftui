//
//  ContentView.swift
//  MiCard-SwiftUI
//
//  Created by Bin on 19/04/2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack {
            Color(red: 0.09, green: 0.63, blue: 0.52)
                .edgesIgnoringSafeArea(.all)
            VStack {
                Image("m1-ultra")
                    .resizable().aspectRatio(contentMode: .fit)
                    .frame(width: 150.0, height: 150.0)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.blue, lineWidth: 3))
                Text("bigSur")
                    .font(Font.custom("Pacifico-Regular", size: 40))
                    .bold()
                    .foregroundColor(.white)
                Text("macOS")
                    .font(.system(size: 25))
                    .foregroundColor(.white)
                Divider()
                InforView(text: "+44 123 456 789", imageName: "phone.fill")
                InforView(text: "bigSur@apple.com", imageName: "mail")
            }
        }
        
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


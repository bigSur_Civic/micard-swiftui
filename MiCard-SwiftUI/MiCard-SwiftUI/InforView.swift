//
//  InforView.swift
//  MiCard-SwiftUI
//
//  Created by Bin on 19/04/2022.
//

import SwiftUI

struct InforView: View {
    let text: String
    let imageName: String
    var body: some View {
        RoundedRectangle(cornerRadius: 20)
            .fill(.white)
            .frame(height: 50)
            .overlay(HStack{
                Image(systemName: imageName)
                    .foregroundColor(.green)
                Text(text)
                
            })
            .padding(.all)
    }
}


struct InforView_Previews: PreviewProvider {
    static var previews: some View {
        InforView(text: "+55 987 654 321", imageName: "applelogo")
            .previewLayout(.sizeThatFits)
    }
}

//
//  MiCard_SwiftUIApp.swift
//  MiCard-SwiftUI
//
//  Created by Bin on 19/04/2022.
//

import SwiftUI

@main
struct MiCard_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
